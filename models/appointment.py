from datetime import date, timedelta
from odoo import fields, models, api


class Appointment(models.Model):
    _name = "appointment.info"
    patient_name = fields.Char(required = True)
    #service_type = fields.Selection([('Dental','Dental'),('Eye examination','Eye examination'), ('Laboratory test','Laboratory test'), ('Emergency','Emergency')]) 
    services = fields.Many2one('service.info',
        ondelete='cascade', string="Service", required=True)
    date_from = fields.Datetime(index=True, copy=False,track_visibility='onchange')
    date_to = fields.Datetime(copy=False,track_visibility='onchange')
    duration= fields.Char(compute='_compute_duration',track_visibility='onchange')
    duration_temp= fields.Float(copy=False, readonly=False)
    employee_ids = fields.Many2many('hr.employee', 'appointment_employee_rel', 'appointment_id', 'emp_id', string='Employees')

    @api.multi
    @api.depends('duration_temp')
    def _compute_duration(self):
        for appo in self:
            hours=int(appo.duration_temp //3600)
            mins=int(appo.duration_temp /3600%1*60)
            appo.duration=str(hours).zfill(2)+":"+str(mins).zfill(2)
         

    @api.onchange('date_from')  
    def _onchange_date_from(self):
        date_from=self.date_from 
        date_to=self.date_to 

        if date_from and not date_to:
            date_to_with_delta= fields.Datetime.from_string(date_from)+timedelta(hours=1)
            self.date_to=str(date_to_with_delta)

        if (date_to and date_from) and (date_from <= date_to):
            self.duration_temp=self._get_hours(date_from,date_to)
        else:
            self.duration_temp=0    

    def _get_hours(self,date_from,date_to):
        from_dt=fields.Datetime.from_string(date_from)
        to_dt=fields.Datetime.from_string(date_to)
        time_delta= to_dt-from_dt
        return time_delta.seconds


    @api.onchange('date_to')  
    def _onchange_date_to(self):
        date_from=self.date_from 
        date_to=self.date_to 

        if (date_to and date_from) and (date_from <= date_to):
            self.duration_temp=self._get_hours(date_from,date_to)
        else:
            self.duration_temp=0            



       

class Service(models.Model):
    _name = "service.info"
    name = fields.Char()